/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_page_configuration.vala
 *
 * Copyright 2019 Fernando Fernandez <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_page_player.ui" )]
  public class PagePlayer : Gtk.Grid {
    [GtkChild]
    private AlbumsViewer albums_viewer;
    [GtkChild]
    private SongsViewer songs_viewer;
    [GtkChild]
    private Playlist playlist;
    [GtkChild]
    private Player player;

    construct {}

    public void load_albums () {
      this.albums_viewer.load_albums ();
    }

    public void move_album_selection ( int i ) {
      this.albums_viewer.move_album_selection ( i );
    }

    public void move_song_selection ( int i ) {
      this.songs_viewer.move_song_selection ( i );
    }

    public void add_song_to_playlist () {
      this.playlist.add_song_to_playlist ( this.songs_viewer.get_selected_song_id () );
    }

    public void restore_playlist () {
      this.playlist.restore_playlist ();
    }

    [GtkCallback]
    private void load_album_songs () {
      this.songs_viewer.load_album_songs ( this.albums_viewer.get_album_selected_id () );
    }

    [GtkCallback]
    private void play_song () {
      if ( this.playlist.has_songs () && this.player.is_idle () ) {
        this.player.play_song ( this.playlist.get_first_song_id () );
        this.playlist.remove_first_song ();
      }
    }
  }
}
