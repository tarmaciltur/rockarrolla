/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_page_configuration.vala
 *
 * Copyright 2019-2020 Fernando Fernandez <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_page_player.ui" )]
  public class PagePlayer : Gtk.Widget {
    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned AlbumsViewer albums_viewer;
    [GtkChild]
    private unowned SongsViewer songs_viewer;
    [GtkChild]
    private unowned Playlist playlist;
    [GtkChild]
    private unowned Gtk.Label label_songs_per_dime;
    [GtkChild]
    private unowned Gtk.Label label_songs_left;
    [GtkChild]
    private unowned PageIdleCover page_idle_cover;

    construct {
      this.set_name ( "pageplayer" );
    }

    public void albums_load_start () {
      this.albums_viewer.albums_load_start ();
    }

    public void load_album ( Album album ) {
      this.albums_viewer.load_album ( album );
    }

    public void albums_load_end () {
      this.albums_viewer.albums_load_end ();
    }

    public void move_album_selection ( int i ) {
      this.albums_viewer.move_album_selection ( i );
    }

    public void move_song_selection ( int delta ) {
      this.songs_viewer.move_song_selection ( delta );
    }

    public string get_selected_song_id () {
      return this.songs_viewer.get_selected_song_id ();
    }

    [GtkCallback]
    private void load_album_songs () {
      this.songs_viewer.load_album_songs ( this.albums_viewer.get_album_selected () );
    }

    public void set_songs_per_dime ( string songs_per_dime, string dime_cost ) {
      string template = _("%s songs for %s" );
      this.label_songs_per_dime.set_text ( template.printf ( songs_per_dime, dime_cost ) );
    }

    public void set_songs_left ( string songs_left ) {
      string template = _("%s songs left to choose" );

      this.label_songs_left.set_text ( template.printf ( songs_left ) );
    }

    public void set_idle ( bool idle ) {
      if ( idle ) {
        this.stack.set_visible_child_name ( "page_idle" );
      } else {
        this.stack.set_visible_child_name ( "page_player" );
      }
    }

    public void playlist_update_songs ( ListStore songs ) {
      this.playlist.update_songs ( songs );
      this.page_idle_cover.set_songs_left ( this.playlist.songs_in_playlist () );
    }

    public void playlist_update_time ( int time_left, int time_elapsed ) {
      this.playlist.update_time ( time_left, time_elapsed );
      this.page_idle_cover.update_time ( time_elapsed );
      this.page_idle_cover.set_total_time_left ( time_left );
    }

    public void idle_cover_update ( string title, string artist, string path, double length ) {
      this.page_idle_cover.update ( title, artist, path, length );
    }
  }
}
