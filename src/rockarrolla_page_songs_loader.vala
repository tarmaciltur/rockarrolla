/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_page_configuration.vala
 *
 * Copyright 2019 Andres Fernandez <andres@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_page_songs_loader.ui" )]
  public class PageSongsLoader : Gtk.Grid {
    [GtkChild]
    private unowned Gtk.Button button_reload_collection;
    [GtkChild]
    private unowned Gtk.Label label_status;
    [GtkChild]
    private unowned Gtk.Label label_current_action;
    [GtkChild]
    private unowned Gtk.ProgressBar progress_bar;
    private uint pulse_timeout_id;

    public signal void reload_collection ();

    construct {
      this.pulse_timeout_id = 0;
    }

    public void set_button_reload_sensitive ( bool state ) {
      this.button_reload_collection.set_sensitive ( state );
    }

    [GtkCallback]
    private void reload_collection_start () {
      this.button_reload_collection.set_sensitive ( false );
      this.reload_collection ();
    }

    public void update_progress ( string status, string action, float progress ) {
      this.label_status.set_text ( status );
      this.label_current_action.set_text ( action );

      switch ( (int)progress ) {
        case Rockolla.Pulse.START:
          this.pulse_start ();
          break;
        case Rockolla.Pulse.STOP:
          this.pulse_stop ();
          break;
        default:
          this.update_progress_bar ( progress );
          break;
      }
    }

    private void update_progress_bar ( float progress ) {
      this.progress_bar.set_fraction ( progress );
      this.progress_bar.set_text ( ( (int)(progress * 100) ).to_string () + "%" );
    }

    private void pulse_start () {
      if ( this.pulse_timeout_id == 0 ) {
        this.progress_bar.set_text ( "" );
        this.pulse_timeout_id = Timeout.add ( 100, this.pulse );
      }
    }

    private bool pulse () {
      this.progress_bar.pulse ();

      return true;
    }

    private void pulse_stop () {
      Source.remove ( this.pulse_timeout_id );
    }

    public void reload_finished () {
      this.button_reload_collection.set_sensitive ( true );
    }
  }
}
