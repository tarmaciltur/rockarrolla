/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_page_configuration.vala
 *
 * Copyright 2019 Andres Fernandez <andres@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_page_songs_loader.ui" )]
  public class PageSongsLoader : Gtk.Grid {
    [GtkChild]
    private Gtk.Button button_reload_collection;
    [GtkChild]
    private Gtk.Label label_status;
    [GtkChild]
    private Gtk.Label label_current_action;
    [GtkChild]
    private Gtk.ProgressBar progress_bar;

    private Array<string> songs_path;

    construct {
      this.songs_path = new Array<string> ();
    }

    public void scan_directory ( string directory ) {
      string name = "";

      try {
        Dir dir = Dir.open ( directory, 0 );

        while ( ( name = dir.read_name () ) != null ) {
          string path = Path.build_filename (directory, name);

          if ( FileUtils.test ( path, FileTest.IS_REGULAR ) ) {
            var ext_index = path.last_index_of ( "." );
            var extension = path.substring ( ext_index );

            if ( extension.down () == ".mp3" ) {
              this.songs_path.append_val ( path );
            }
          }

          if ( FileUtils.test ( path, FileTest.IS_DIR ) ) {
            this.scan_directory ( path );
          }
        }
      } catch ( FileError err ) {
        stderr.printf ( err.message );
      }
    }

    [GtkCallback]
    private void reload_collection () {
      this.button_reload_collection.set_sensitive ( false );

      var settings = new Settings ( "ar.com.softwareperonista.Rockarrolla" );
      var music_directory = settings.get_string ( "music-directory" );

      this.scan_directory ( music_directory );

      this.insert_albums ();
      this.insert_songs ();

      this.remove_non_existent_songs ();
      this.remove_non_existent_albums ();

      this.label_status.set_label ( _("Collection reloaded") );
      this.label_current_action.set_label ( "" );

      this.button_reload_collection.set_sensitive ( true );
    }

    private void insert_albums () {
      this.label_status.set_label ( _("Adding albums") );

      for (int i = 0; i < this.songs_path.length; i++ ) {
        this.label_current_action.set_label ( _("Checking file ") + this.get_filename_from_path ( this.songs_path.index ( i ) ) );

        var file = new TagLib.File ( this.songs_path.index ( i ) );

        var album = file.tag.album.replace ("\"", "");

        if ( album != "" && ApplicationWindow.db.select ( "albums","name", "WHERE name = \"" + album +"\"" ).length == 0 ) {
          var artist = file.tag.artist.replace ("\"", "");

          if ( artist != "" ) {
            ApplicationWindow.db.insert ( "albums","name,artist,cover", "\"" + album + "\", \"" + artist + "\", \"cover\"" );
          }
        }

        this.progress_bar.set_fraction ( (double) i / (double) this.songs_path.length );
        this.progress_bar.set_text ( ((int)(((double)i / (double)this.songs_path.length ) * 100)).to_string () + "%");

        while ( Gtk.events_pending () ) {
          Gtk.main_iteration ();
        }

        this.progress_bar.set_fraction ( 1.0 );
        this.progress_bar.set_text ( "100%");
      }
    }

    private void insert_songs () {
      this.label_status.set_label ( _("Adding songs") );

      for (int i = 0; i < this.songs_path.length; i++ ) {
        this.label_current_action.set_label ( _("Checking file ") + this.get_filename_from_path ( this.songs_path.index ( i ) ) );

        var file = new TagLib.File ( this.songs_path.index ( i ) );

        var album = file.tag.album.replace ("\"", "");

        var id = ApplicationWindow.db.select ( "albums","id", "WHERE name = \"" + album +"\"" ).index ( 0 );
        var title = file.tag.title.replace ("\"", "");

        if ( id != null && ApplicationWindow.db.select ( "songs","title,album,path", "WHERE title = \"" + title + "\" AND album = \"" + id + "\" AND path = \"" + this.songs_path.index ( i ) + "\"" ).length == 0 ) {
          var track = file.tag.track;
          var length = file.audioproperties.length;
          ApplicationWindow.db.insert ( "songs","title,track,album,length,path", "\"" + title + "\", \"" + track.to_string ("%02d") + "\", \"" + id + "\", \"" + length.to_string () + "\", \"" + this.songs_path.index ( i ) + "\"" );
        }

        this.progress_bar.set_fraction ( (double) i / (double) this.songs_path.length );
        this.progress_bar.set_text ( ((int)(((double)i / (double)this.songs_path.length ) * 100)).to_string () + "%");

        while ( Gtk.events_pending () ) {
          Gtk.main_iteration ();
        }

        this.progress_bar.set_fraction ( 1.0 );
        this.progress_bar.set_text ( "100%");
      }
    }

    private void remove_non_existent_songs () {
      this.label_status.set_label ( _("Removing non existent songs") );
      var songs = ApplicationWindow.db.select ( "songs", "id,path,title", "" );

      for ( int i = 0; i < songs.length; i++ ) {
        var song = songs.index ( i ).split ("|");

        this.label_current_action.set_label ( _("Checking song ") + song[2] );

        if ( !(Doctrina.Infraestructura.existe_path ( song[1] )) ) {
          debug ( "cancion " + song[1] + " no existe" );
          ApplicationWindow.db.del ( "songs", "WHERE id='" + song[0] + "'");
        }

        this.progress_bar.set_fraction ( (double) i / (double) songs.length );
        this.progress_bar.set_text ( ((int)(((double) i / (double) songs.length ) * 100)).to_string () + "%");

        while ( Gtk.events_pending () ) {
          Gtk.main_iteration ();
        }
      }

      this.progress_bar.set_fraction ( 1.0 );
      this.progress_bar.set_text ( "100%");
    }

    private void remove_non_existent_albums () {
      this.label_status.set_label ( _("Removing non existent albums") );

      var albums = ApplicationWindow.db.select ( "albums", "id,name", "" );

      for ( int i = 0; i < albums.length; i++ ) {

        var album = albums.index(i).split ("|");
        this.label_current_action.set_label ( _("Checking album ") + album[1] );

        if ( ApplicationWindow.db.select ( "songs", "id", "WHERE album='" + album[0] + "'" ).length == 0) {
          ApplicationWindow.db.del ( "albums", "WHERE id='" + album[0] + "'");

          this.progress_bar.set_fraction ( (double) i / (double) albums.length );
          this.progress_bar.set_text ( ((int)(((double) i / (double) albums.length ) * 100)).to_string () + "%");

          while ( Gtk.events_pending () ) {
            Gtk.main_iteration ();
          }
        }
      }

      this.progress_bar.set_fraction ( 1.0 );
      this.progress_bar.set_text ( "100%");
    }

    private string get_filename_from_path ( string path ) {
      int last_slash = path.last_index_of ( "/" );
      return path.substring ( last_slash + 1 );
    }
  }
}
