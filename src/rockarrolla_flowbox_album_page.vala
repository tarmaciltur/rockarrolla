/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_flowbox_album_page.vala
 *
 * Copyright 2019-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_flowbox_album_page.ui" )]
  public class FlowBoxAlbumPage : Gtk.Widget {
    [GtkChild]
    private unowned Gtk.FlowBox flowbox;
    private uint child_count;
    private int window_height;
    public uint length { get; private set;}

    public signal void selected_children_changed ();

    construct {
      this.child_count = 0;
      this.window_height = 0;
      this.length = 0;
    }

    public bool is_full () {
      return this.length == 10;
    }

    public void insert ( FlowBoxChildAlbum album, int position ){
      if ( this.child_count < 10) {
        this.flowbox.insert ( album, position );
        this.child_count++;
        this.remove_can_focus ();
        if ( !album.is_dummy () ) {
          this.length++;
        } else {
          album.get_parent ().set_can_target ( false );
        }
      }
    }

    private void remove_can_focus () {
      var last = this.flowbox.get_last_child ();
      last.set_can_focus ( false );
    }

    public int get_window_height () {
      return this.window_height;
    }

    public int get_selected_child_index () {
      int ret = -1;

      if ( this.flowbox.get_selected_children ().length () == 1 ) {
        var selected_child = this.flowbox.get_selected_children ().nth_data ( 0 );
        ret++;
        var child = this.flowbox.get_first_child ();
        while ( child != null && selected_child != child  ) {
          ret++;
          child = child.get_next_sibling ();
        }
      }

      return ret;
    }

    public string get_selected_album_id () {
      var id = "";
      if ( this.flowbox.get_selected_children ().length () > 0 ) {
        var selected_album = this.flowbox.get_selected_children ().nth_data ( 0 ).child as FlowBoxChildAlbum;
        if ( selected_album != null ) {
          id = selected_album.get_id ();
        }
      }

      return id;
    }

    public Album? get_selected_album () {
      Album album = null;
      var selected_album = this.flowbox.get_selected_children ().nth_data ( 0 ).child as FlowBoxChildAlbum;
      if ( selected_album != null ) {
        album = selected_album.get_album ();
      }

      return album;
    }

    [GtkCallback]
    private void on_selected_children_changed () {
      this.selected_children_changed ();
    }

    public void select_child ( uint index ) {
      int i = 0;

      var child = this.flowbox.get_first_child () as Gtk.FlowBoxChild;

      while ( child != null && i < index ) {
        i++;
        child = child.get_next_sibling () as Gtk.FlowBoxChild;
      }

      if ( child != null ) {
        this.flowbox.select_child ( child );
      }
    }

    public void remove_child () {
      this.flowbox.unparent ();
    }
  }
}
