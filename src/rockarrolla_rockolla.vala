/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_rockolla.vala
 *
 * Copyright 2020 Fernando Fernandez <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class Rockolla : Object {
    public Doctrina.BaseDeDatos db { get; private set; }
    public Doctrina.Infraestructura infra { get; private set; }
    public Array<Song> playlist { get; private set; }
    private Player player;
    private string covers_dir;
    private int time_left;
    private Array<string> songs_path;
    private Settings settings;

    public signal void albums_load_start ();
    public signal void album_loaded ( Album album );
    public signal void albums_load_end ();
    public signal void playlist_empty ();
    public signal void playlist_update_songs ( ListStore songs );
    public signal void playlist_update_time ( int time_left, int time_elapsed );
    public signal void idle_cover_update ( string title, string artist, string path, double length );
    public signal void loader_update ( string status, string action, float progress );
    public signal void collection_reloaded ();
    public signal void update_songs_per_dime ( string songs_per_dime, string dime_cost );
    public signal void updated_songs_left ( string songs_left );

    public Rockolla () {
      this.playlist = new Array<Song> ();
      this.player = new Player ();
      this.songs_path = new Array<string> ();

      this.infra = new Doctrina.Infraestructura (
        "rockarrolla",
        "3",
        "resource:///ar/com/softwareperonista/rockarrolla/db/rockarrolla_base_de_datos_definicion.sql" );
      this.db = new Doctrina.BaseDeDatos ( "db.sqlite",
                                           this.infra );

      this.covers_dir = this.infra.directorio_de_configuracion () + "/albums_covers/";
      if ( !Doctrina.Infraestructura.existe_path ( this.infra.directorio_de_configuracion () + "/albums_covers" ) ) {
        Doctrina.Infraestructura.crear_directorio ( this.infra.directorio_de_configuracion () + "/albums_covers" );
      }

      this.time_left = 0;

      this.settings = new Settings ( "ar.com.softwareperonista.Rockarrolla" );
      this.settings.changed.connect ( this.settings_changed );

      this.player.playing.connect ( this.update_time );
      this.player.play_finished.connect ( this.play_finished );
    }

    private void settings_changed ( string key ) {
      if ( key == "songs-per-dime" || key == "dime-cost" ) {
        int songs = this.settings.get_int ( "songs-per-dime" );
        string dime = this.settings.get_string ( "dime-cost" );

        this.update_songs_per_dime ( songs.to_string (), dime );
      }

      if ( key == "songs-left" ) {
        int songs_left = this.settings.get_int ( "songs-left" );

        this.updated_songs_left ( songs_left.to_string () );
      }
    }

    public void update_info_labels () {
      this.settings_changed ( "dime-cost" );
      this.settings_changed ( "songs-left" );
    }

    public void load_albums () {
      this.albums_load_start ();

      this.load_albums_async.begin ( (obj, res) => {
        this.albums_load_end ();
      } );
    }

    private async void load_albums_async () {
      var albums_db = this.db.select ( "albums", "id,name,artist", "ORDER BY artist ASC, name ASC" );

      for ( int i = 0; i < albums_db.length; i++ ) {
        var album_db = albums_db.index ( i ).split ( "|" );

        var id = album_db[0];
        var name = album_db[1];
        var artist = album_db[2];
        var cover = generate_cover_path ( artist, name );

        if ( !Doctrina.Infraestructura.existe_path ( cover ) ) {
          cover = "";
        }

        var album = new Album ( id, name, artist, cover );
        this.get_album_songs ( album );

        this.album_loaded ( album );
        Idle.add ( this.load_albums_async.callback );
        yield;
      }
    }

    private void get_album_songs ( Album album ) {
      var songs_db = this.db.select ( "songs", "id,title,length,path", "WHERE album = '" + album.id + "' ORDER BY track ASC" );

      for ( int i = 0; i < songs_db.length; i++ ) {
        var song_db = songs_db.index ( i ).split ( "|" );

        var id = song_db[0];
        var title = song_db[1];
        var length = int.parse ( song_db[2] );
        var path = song_db[3];

        var song = new Song ( id, title, length, path );

        album.add_song ( song );
      }

      album.set_total_songs ( songs_db.length );
    }

    public void play () {
      if ( this.playlist.length > 0 ) {
        if ( this.player.state == Player.State.STOPPED ) {
          this.player.play_song ( this.playlist.index ( 0 ) );
          this.update_cover ();
        }
      } else {
        this.playlist_empty ();
      }
    }

    public void add_songs () {
      int songs_per_dime = this.settings.get_int ( "songs-per-dime" );
      int songs_left = this.settings.get_int ( "songs-left" );

      this.settings.set_int ( "songs-left", songs_left + songs_per_dime );
    }

    public void add_song ( string id ) {
      if ( this.settings.get_int ( "songs-left" ) > 0 ) {
        this.db.insert ( "playlist", "song", "'" + id + "'" );

        if ( this.playlist_add_song ( id ) ) {
          int songs_left = this.settings.get_int ( "songs-left" );
          this.settings.set_int ( "songs-left", songs_left - 1 );
        }
      }
    }

    private bool playlist_add_song ( string id ) {
      debug ( "id: %s", id );
      var ret = true;

      if ( !this.song_in_playlist ( id ) ) {
        var song = this.create_song ( id );
        this.playlist.append_val ( song );

        this.time_left += song.length;
        this.play ();
        this.update_playlist ();
      } else {
        ret = false;
      }

      return ret;
    }

    private bool song_in_playlist ( string id ) {
      var found = false;
      for ( uint i = 0; i < this.playlist.length; i++ ) {
        var song = this.playlist.index ( i );
        if ( song != null && song.id == id ) {
          found = true;
          break;
        }
      }

      return found;
    }

    private Song create_song ( string id ) {
      var song_db = this.db.select ( "songs,albums", "title,length,path,artist", "WHERE songs.id = '" + id + "' AND songs.album = albums.id" ).index ( 0 ).split ( "|" );

      var title = song_db[0];
      var length = int.parse ( song_db[1] );
      var path = song_db[2];
      var artist = song_db[3];

      var song = new Song ( id, title, length, path, artist );

      return song;
    }

    private void play_finished () {
      var time = this.db.select ( "playlist", "min(time)", "WHERE song = '"+ this.playlist.index ( 0 ).id + "'" ).index ( 0 );
      this.db.del ( "playlist", "WHERE song = '" + this.playlist.index ( 0 ).id + "' AND time = '" + time + "'" );
      this.time_left -= this.playlist.index ( 0 ).length;
      this.playlist.remove_index ( 0 );
      this.update_playlist ();
      if ( time_left != 0 ) {
        this.update_time ( 0 );
      }
      this.play ();
    }

    public void restore_playlist () {
      var songs_id = this.db.select ( "playlist", "song", "" );

      for ( int i = 0; i < songs_id.length; i++ ) {
        this.playlist_add_song ( songs_id.index ( i ) );
      }
    }

    private void update_playlist () {
      var liststore = new ListStore ( typeof(Song) );

      for ( int i = 0; i < this.playlist.length; i++ ) {
        liststore.append ( this.playlist.index ( i ) );
      }

      this.playlist_update_songs ( liststore );
    }

    private void update_time ( int time_elapsed ) {
      var real_time_left = this.time_left - time_elapsed;
      this.playlist_update_time ( real_time_left, time_elapsed );
    }

    private void update_cover () {
      if ( this.playlist.length > 0 ) {
        var song = this.playlist.index ( 0 );
        var title = song.title;
        var length = song.length;
        var album = this.db.select ( "songs, albums",
                                     "artist,name",
                                     "WHERE songs.id = '" + song.id + "' AND album = albums.id" ).index ( 0 ).split ( "|" );
        var artist = album[0];
        var name = album[1];

        var cover = generate_cover_path ( artist, name );

        if ( !Doctrina.Infraestructura.existe_path ( cover ) ) {
          cover = "";
        }

        this.idle_cover_update ( title, artist, cover, length );
      } else {
        this.idle_cover_update ( "", "", "", 0 );
      }
    }

    public void toggle_pause () {
      this.player.toggle_pause ();
    }

    public void skip_song () {
      if ( this.player.state != Player.State.STOPPED ) {
        this.player.stop ();
      }
    }

    private string generate_cover_path ( string artist, string name ) {
      return this.covers_dir + CoverFetcher.sha1 ( artist + "_" + name ) + ".png";
    }

    public bool is_playing () {
      return this.player.state == Player.State.PLAYING;
    }

    public void reload_collection () {
      this.reload_collection_async.begin ( (obj, res) => {
        this.collection_reloaded ();
        this.load_albums ();
      } );
    }

    private async void reload_collection_async () {
      var settings = new Settings ( "ar.com.softwareperonista.Rockarrolla" );
      var music_directory = settings.get_string ( "music-directory" );

      yield this.scan_directory_async ( music_directory );
      this.loader_update ( "Searching for music files", "", (float)Rockolla.Pulse.STOP );

      yield this.insert_albums_async ();
      yield this.insert_songs_async ();

      yield this.remove_songs_out_of_music_directory_async ();
      yield this.remove_non_existent_songs_async ();
      yield this.remove_empty_albums_async ();

      yield this.fetch_cover_album_async ();

      this.loader_update ( _("Collection reloaded" ), "", 1 );
    }

    public async void scan_directory_async ( string directory ) {
      string name = "";
      string status = "Searching for music files";
      var settings = new Settings ( "ar.com.softwareperonista.Rockarrolla" );
      var music_directory = settings.get_string ( "music-directory" );

      try {
        Dir dir = Dir.open ( directory, 0 );

        while ( ( name = dir.read_name () ) != null ) {
          string path = Path.build_filename ( directory, name );
          this.loader_update ( status, path.replace ( music_directory, "..." ), (float)Rockolla.Pulse.START );
          Idle.add ( this.scan_directory_async.callback );
          yield;

          if ( FileUtils.test ( path, FileTest.IS_REGULAR ) ) {
            var ext_index = path.last_index_of ( "." );
            var extension = path.substring ( ext_index );

            switch ( extension.down () ) {
              case ".mp3":
              case ".flac":
                this.songs_path.append_val ( path );
                break;
            }
          }

          if ( FileUtils.test ( path, FileTest.IS_DIR ) ) {
            yield this.scan_directory_async ( path );
          }
        }
      } catch ( FileError err ) {
        stderr.printf ( err.message );
      }
    }

    private async void insert_albums_async () {
      string status = _("Adding albums" );
      string previous_album = "";

      for ( int i = 0; i < this.songs_path.length; i++ ) {
        string action = _("Checking file " ) + this.get_filename_from_path ( this.songs_path.index ( i ) );

        var file = new TagLib.File ( this.songs_path.index ( i ) );

        var album = file.tag.album.replace ( "\"", "" );

        if ( album != ""  && album != previous_album && this.db.select ( "albums", "name", "WHERE name = \"" + album +"\"" ).length == 0 ) {
          var artist = file.tag.artist.replace ( "\"", "" );

          if ( artist != "" ) {
            this.db.insert ( "albums", "name,artist,cover", "\"" + album + "\", \"" + artist + "\", \"cover\"" );
          }
        }
        previous_album = album;

        float progress = (float)i / (float)this.songs_path.length;
        this.loader_update ( status, action, progress );

        Idle.add ( this.insert_albums_async.callback );
        yield;
      }

      this.loader_update ( status, _("Done" ), 1 );
      Idle.add ( this.insert_albums_async.callback );
      yield;
    }

    private async void insert_songs_async () {
      string status = _("Adding songs" );

      for ( int i = 0; i < this.songs_path.length; i++ ) {
        string action = _("Checking file " ) + this.get_filename_from_path ( this.songs_path.index ( i ) );

        var file = new TagLib.File ( this.songs_path.index ( i ) );

        var album = file.tag.album.replace ( "\"", "" );

        var id = this.db.select ( "albums", "id", "WHERE name = \"" + album +"\"" ).index ( 0 );
        var title = file.tag.title.replace ( "\"", "" );

        if ( id != null && this.db.select ( "songs", "title,album,path", "WHERE title = \"" + title + "\" AND album = \"" + id + "\" AND path = \"" + this.songs_path.index ( i ) + "\"" ).length == 0 ) {
          var track = file.tag.track;
          var length = file.audioproperties.length;
          this.db.insert ( "songs", "title,track,album,length,path", "\"" + title + "\", \"" + track.to_string ( "%02d" ) + "\", \"" + id + "\", \"" + length.to_string () + "\", \"" + this.songs_path.index ( i ) + "\"" );
        }

        float progress = (float)i / (float)this.songs_path.length;
        this.loader_update ( status, action, progress );
        Idle.add ( this.insert_songs_async.callback );
        yield;
      }

      this.loader_update ( status, _("Done" ), 1 );
      Idle.add ( this.insert_songs_async.callback );
      yield;
    }

    private async void remove_non_existent_songs_async () {
      string status = _("Removing non existent songs" );
      var songs = this.db.select ( "songs", "id,path,title", "" );

      for ( int i = 0; i < songs.length; i++ ) {
        var song = songs.index ( i ).split ( "|" );
        string action = _("Checking song " ) + song[2];

        if ( !(Doctrina.Infraestructura.existe_path ( song[1] ) ) ) {
          debug ( "cancion " + song[1] + " no existe" );
          this.db.del ( "songs", "WHERE id='" + song[0] + "'" );
        }

        float progress = (float)i / (float)songs.length;
        this.loader_update ( status, action, progress );
        Idle.add ( this.remove_non_existent_songs_async.callback );
        yield;
      }

      this.loader_update ( status, _("Done" ), 1 );
      Idle.add ( this.remove_non_existent_songs_async.callback );
      yield;
    }

    private async void remove_songs_out_of_music_directory_async () {
      string status = _("Removing non existent songs" );

      var settings = new Settings ( "ar.com.softwareperonista.Rockarrolla" );
      var music_directory = settings.get_string ( "music-directory" );

      var songs = this.db.select ( "songs", "id,path", "" );

      for ( int i = 0; i < songs.length; i++ ) {
        var song = songs.index ( i ).split ( "|" );
        string action = _("Checking song " ) + song[2];

        var path = song[1];
        if ( !( path.length > music_directory.length &&
                path.substring ( 0, music_directory.length ) == music_directory) ) {
          this.db.del ( "songs", "WHERE id='" + song[0] + "'" );
        }

        float progress = (float)i / (float)songs.length;
        this.loader_update ( status, action, progress );
        Idle.add ( this.remove_songs_out_of_music_directory_async.callback );
        yield;
      }

      this.loader_update ( status, _("Done" ), 1 );
      Idle.add ( this.remove_songs_out_of_music_directory_async.callback );
      yield;
    }

    private async void remove_empty_albums_async () {
      string status = _("Removing empty albums" );

      var albums = this.db.select ( "albums", "id,name", "" );

      for ( int i = 0; i < albums.length; i++ ) {
        var album = albums.index ( i ).split ( "|" );
        string action = _("Checking album " ) + album[1];

        if ( this.db.select ( "songs", "id", "WHERE album='" + album[0] + "'" ).length == 0 ) {
          this.db.del ( "albums", "WHERE id='" + album[0] + "'" );

        }

        float progress = (float)i / (float)albums.length;
        this.loader_update ( status, action, progress );
        Idle.add ( this.remove_empty_albums_async.callback );
        yield;
      }

      this.loader_update ( status, _("Done" ), 1 );
      Idle.add ( this.remove_empty_albums_async.callback );
      yield;
    }

    private async void fetch_cover_album_async () {
      string status = _("Searching album covers" );
      var albums = this.db.select ( "albums", "artist,name,id", "ORDER BY artist ASC, name ASC" );
      for ( int i = 0; i < albums.length; i++ ) {
        var album = albums.index ( i ).split ( "|" );
        string artist = album[0];
        string name = album[1];
        string id = album[2];

        string action = _("Looking for cover of " ) + album[1];

        var album_songs = this.db.select ( "songs", "path", "WHERE album = '" + id + "' ORDER BY track ASC" );

        string path = album_songs.index ( 0 );

        var fetcher = new CoverFetcher ( this.infra, artist, name, path );
        yield fetcher.fetch_async ();

        float progress = (float)i / (float)albums.length;
        this.loader_update ( status, action, progress );
        Idle.add ( this.fetch_cover_album_async.callback );
        yield;
      }

      this.loader_update ( status, "Done", 1 );
      Idle.add ( this.fetch_cover_album_async.callback );
      yield;
    }

    private string get_filename_from_path ( string path ) {
      int last_slash = path.last_index_of ( "/" );
      return path.substring ( last_slash + 1 );
    }

    public enum Pulse {
      START = -1,
      STOP = -2
    }
  }
}
