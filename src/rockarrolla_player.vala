/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_player.vala
 *
 * Copyright 2019 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_player.ui" )]
  public class Player : Gtk.Grid {
    [GtkChild]
    private Gtk.ProgressBar progress_bar;
    [GtkChild]
    private Gtk.Label label_time_elapsed;
    [GtkChild]
    private Gtk.Label label_time_left;
    [GtkChild]
    private Gtk.Label label_artist_title;
    private uint timeout_id;
    private int length;
    private int time_elapsed;
    private bool player_idle;
    private Gst.Element pipeline;
    private string current_playing_song_id;

    construct {
      this.length = 0;
      this.time_elapsed = 0;
      this.timeout_id = 0;
      this.player_idle = true;
      this.pipeline  = Gst.ElementFactory.make ( "playbin", "player" );
    }

    public signal void play_finished ();


    public void play_song ( string id ) {

      var song = ApplicationWindow.db.select ( "songs,albums", "path,length,artist,title", "WHERE songs.id = '" + id + "' AND albums.id = songs.album" ).index ( 0 ).split ( "|" );
      string path = song[0];
      debug ( path );
      this.length = int.parse ( song[1] );
      debug ( song[1] );
      this.time_elapsed = 0;
      this.current_playing_song_id = id;

      this.label_time_elapsed.set_text ( "00:00" );
      this.label_time_left.set_text ( new DateTime.from_unix_utc ( this.length ).format ( "%M:%S" ) );
      this.label_artist_title.set_text ( song[2] + " - " + song[3] );

      this.pipeline.set_state ( Gst.State.NULL );
      this.pipeline.set ( "uri", "file://" + path );

      var bus = pipeline.get_bus ();
      bus.add_watch ( GLib.Priority.DEFAULT, on_bus_callback );

      this.pipeline.set_state ( Gst.State.PLAYING );
      this.player_idle = false;

      this.timeout_id = Timeout.add_seconds ( 1, update_progress_bar );
    }

    bool on_bus_callback ( Gst.Bus bus, Gst.Message message )
    {
      if ( message.type == Gst.MessageType.ERROR || message.type == Gst.MessageType.EOS )
      {
        this.pipeline.set_state ( Gst.State.NULL );
        this.player_idle = true;

        this.length = 0;
        this.update_progress_bar ();
        this.label_artist_title.set_text ( "" );

        if ( this.timeout_id != 0 ) {
          Source.remove ( this.timeout_id );
        }

        string time = ApplicationWindow.db.select ( "playlist", "time", "WHERE song='" + this.current_playing_song_id + "' ORDER BY time ASC" ).index ( 0 );
        ApplicationWindow.db.del ( "playlist", "WHERE song='" + this.current_playing_song_id + "' AND time='" + time + "'" );

        this.play_finished ();
      }

      return true;
    }

    public bool is_idle () {
      return this.player_idle;
    }

    public bool update_progress_bar () {
      if ( this.length != 0 ) {
        this.time_elapsed++;

        this.progress_bar.set_fraction ( (double)this.time_elapsed / (double)this.length );
      } else {
        this.time_elapsed = 0;
      }

      this.label_time_elapsed.set_text ( new DateTime.from_unix_utc ( this.time_elapsed ).format ( "%M:%S" ) );
      this.label_time_left.set_text ( new DateTime.from_unix_utc ( this.length - this.time_elapsed ).format ( "%M:%S" ) );

      return true;
    }
  }
}
