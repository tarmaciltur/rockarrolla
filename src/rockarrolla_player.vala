/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_player.vala
 *
 * Copyright 2019-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class Player : Object {
    private uint timeout_id;
    private int length;
    public Player.State state { get; private set; }
    private Gst.Element pipeline;

    construct {
      this.length = 0;
      this.timeout_id = 0;
      this.state = Player.State.STOPPED;
    }

    public signal void play_finished ();
    public signal void playing ( int time_elapsed );
    public signal void play_started ();

    public void play_song ( Song song ) {
      if ( this.pipeline == null ) {
        this.pipeline = Gst.ElementFactory.make ( "playbin", "player" );
      }

      if ( this.pipeline != null ) {
        this.pipeline.set_state ( Gst.State.NULL );
        var path = song.path.replace ( "\\", "/" );
        debug ( "starting to play: %s\n", "file:///" + Uri.escape_string ( path, ":/" ) );
        this.pipeline.set ( "uri", "file:///" + Uri.escape_string ( path, ":/" ) );

        var bus = pipeline.get_bus ();
        bus.add_watch ( GLib.Priority.DEFAULT, on_bus_callback );

        this.pipeline.set_state ( Gst.State.PLAYING );
        this.state = Player.State.PLAYING;

        this.timeout_id = Timeout.add ( 100, update_progress );
        this.play_started ();
        this.length = song.length;
      } else {
        message ( "Pipeline is not valid " );
      }
    }

    bool on_bus_callback ( Gst.Bus bus, Gst.Message message )
    {
      if ( message.type == Gst.MessageType.ERROR || message.type == Gst.MessageType.EOS )
      {
        this.stop ();
      }

      return true;
    }

    public void toggle_pause () {
      Gst.State state;
      Gst.State pending;
      Gst.ClockTime timeout = 1000;

      this.pipeline.get_state ( out state, out pending, timeout );
      if ( state == Gst.State.PLAYING ) {
        this.pipeline.set_state ( Gst.State.PAUSED );
        this.state = Player.State.PAUSED;
      } else {
        if ( state == Gst.State.PAUSED ) {
          this.pipeline.set_state ( Gst.State.PLAYING );
          this.timeout_id = Timeout.add ( 100, update_progress );
          this.state = Player.State.PLAYING;
        }
      }
    }

    public void stop () {
      this.pipeline.set_state ( Gst.State.NULL );
      this.state = Player.State.STOPPED;

      this.length = 0;

      if ( this.timeout_id != 0 ) {
        Source.remove ( this.timeout_id );
      }

      this.play_finished ();
    }

    private bool update_progress () {
      int time_elapsed = 0;
      Gst.ClockTime absolute_time = 0;

      var base_time = this.pipeline.get_base_time ();
      var clock = this.pipeline.get_clock ();
      if ( clock != null ) {
        absolute_time = clock.get_time ();
      }

      if ( this.length != 0 ) {
        time_elapsed = (int)( (absolute_time - base_time) / 1000000000);
      } else {
        time_elapsed = 0;
      }

      this.playing ( time_elapsed );

      return true;
    }

    public enum State {
      PLAYING,
      PAUSED,
      STOPPED
    }
  }
}
