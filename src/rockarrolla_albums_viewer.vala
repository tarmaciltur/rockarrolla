/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_scrolled_window_albums.vala
 *
 * Copyright 2019 Fernando Fernandez <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_albums_viewer.ui" )]
  public class AlbumsViewer : Gtk.Grid {
    [GtkChild]
    private Gtk.FlowBox flowbox_albums;
    [GtkChild]
    private Gtk.ScrolledWindow scrolled_window_albums;
    private int album_selected;
    private int album_step;

    public signal void album_selected_changed ();

    public AlbumsViewer () {
      this.scrolled_window_albums.get_vadjustment ().changed.connect ( upper_changed );
      this.load_albums ();
    }

    private void upper_changed () {
      debug ( "upper_changed: %.2f", this.scrolled_window_albums.get_vadjustment ().upper );
    }

    private void clear_flowbox () {
      Gtk.FlowBoxChild child = this.flowbox_albums.get_child_at_index ( 0 );

      while ( child != null ) {
        child.destroy ();
        child = this.flowbox_albums.get_child_at_index ( 0 );
      }

      this.album_step = 0;
      this.album_selected = -1;
    }

    public void load_albums () {
      this.clear_flowbox ();
      var albums = ApplicationWindow.db.select ( "albums", "artist,name,id", "ORDER BY artist ASC, name ASC" );

      for ( int i = 0; i < albums.length; i++ ) {
        var album = albums.index ( i ).split ( "|" );
        var flowbox_child = new Rockarrolla.FlowBoxChildAlbum ();

        flowbox_child.set_artist_name ( album[0] );
        flowbox_child.set_name ( album[1] );
        flowbox_child.set_id ( album[2] );
        flowbox_child.set_image ( "" );

        this.flowbox_albums.insert ( flowbox_child, -1 );
      }

      this.get_toplevel ().show_all ();
      this.move_album_selection ( 1 );
    }

    private void load_album_step () {
      var albums = ApplicationWindow.db.select ( "albums", "artist,name,id", "ORDER BY artist ASC, name ASC" );
      var adjustment = this.scrolled_window_albums.get_vadjustment ();

      int rows = (int)albums.length / 5;
      if ( ( albums.length % 5 ) != 0 ) {
        rows++;
      }

      this.album_step = ( (int)adjustment.upper / rows) * 2;
      debug ( "adjustment.upper: " + adjustment.upper.to_string () );
      debug ( "albums.length: " + albums.length.to_string () );
      debug ( "rows: " + rows.to_string () );
      debug ( "estimated increment: " + ( (adjustment.upper / rows)*2).to_string () );

      this.scrolled_window_albums.set_min_content_height ( this.album_step );
    }

    public void move_album_selection ( int i ) {
      bool scroll = false;
      var children = this.flowbox_albums.get_children ();

      var child = children.nth_data ( this.album_selected + i ) as Gtk.FlowBoxChild;
      if ( child != null ) {
        this.flowbox_albums.select_child ( child );

        this.album_selected += i;
        this.album_selected_changed ();

        if ( i == 1 ) {
          if ( this.album_selected % 10 == 0 ) {
            scroll = true;
          }
        } else {
          if ( this.album_selected % 10 == 9 ) {
            scroll = true;
          }
        }

        while ( this.album_step == 0 ) {
          this.flowbox_albums.queue_draw ();
          while ( Gtk.events_pending () ) {
            Gtk.main_iteration ();
          }
          this.load_album_step ();
        }

        if ( scroll == true ) {
          var adjustment = this.scrolled_window_albums.get_vadjustment ();

          if ( this.album_selected == 0 ) {
            adjustment.set_value ( 0 );
          } else {
            adjustment.set_value ( adjustment.value + (this.album_step * i) );
          }
        }
      }
    }

    public string get_album_selected_id () {
      var children = this.flowbox_albums.get_children ();

      var child = children.nth_data ( this.album_selected ) as Gtk.FlowBoxChild;

      var album = child.get_child () as FlowBoxChildAlbum;

      return album.get_id ();
    }
  }
}
