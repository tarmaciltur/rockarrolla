/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_scrolled_window_albums.vala
 *
 * Copyright 2019-2020 Fernando Fernandez <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_albums_viewer.ui" )]
  public class AlbumsViewer : Gtk.Grid {
    [GtkChild]
    private unowned Gtk.Stack flowbox_pages;
    private int window_height;
    private int pages;

    public signal void album_selected_changed ();

    construct {
      this.window_height = 0;
      this.pages = 0;
    }

    public void albums_load_start () {
      var child = this.flowbox_pages.get_first_child () as FlowBoxAlbumPage;;

      while ( child != null ) {
        child.remove_child ();
        this.flowbox_pages.remove ( child );
        child = this.flowbox_pages.get_first_child () as FlowBoxAlbumPage;
      }

      this.pages = 0;
      this.add_page ();
    }

    private void add_page () {
      var new_page = new FlowBoxAlbumPage ();
      new_page.selected_children_changed.connect ( this.on_selected_children_changed );

      this.pages++;
      this.flowbox_pages.add_named ( new_page, this.pages.to_string () );
    }

    public void load_album ( Album album ) {
      var flowbox_child = new FlowBoxChildAlbum ();

      flowbox_child.set_album ( album );

      var last_page = this.get_last_page ();
      last_page.insert ( flowbox_child, -1 );

      if ( this.pages == 1 && this.get_selected_album_index () == -1 ) {
        this.move_album_selection ( 0 );
      }
    }

    public void albums_load_end () {
      var last_page = this.get_last_page ();
      var dummy_albums = 10 - last_page.length;
      for ( int i = 0; i < dummy_albums; i++ ) {
        var flowbox_child = new FlowBoxChildAlbum ();
        flowbox_child.set_dummy ();
        last_page.insert ( flowbox_child, -1 );
      }
    }

    private FlowBoxAlbumPage get_last_page () {
      if ( this.flowbox_pages.get_first_child () == null ) {
        this.add_page ();
      }

      FlowBoxAlbumPage last_page = this.flowbox_pages.get_last_child () as FlowBoxAlbumPage;

      if ( last_page.is_full () ) {
        this.add_page ();
      }

      return this.flowbox_pages.get_last_child () as FlowBoxAlbumPage;
    }

    private void select_child ( int index ) {
      var page = this.flowbox_pages.get_visible_child () as FlowBoxAlbumPage;

      if ( index >= 0 && page.length > index ) {
        page.select_child ( (uint)index );
      }
    }

    public void move_album_selection ( int delta ) {
      var current_page = this.flowbox_pages.get_visible_child () as FlowBoxAlbumPage;
      int index_current_album = this.get_selected_album_index ();

      if ( index_current_album != -1 ) {
        if ( (index_current_album + delta) < 0 ||
             (index_current_album + delta) > (current_page.length -1) ) {
          this.change_page ( delta );
          this.album_selected_changed ();
        } else {
          this.select_child ( index_current_album + delta );
        }
      } else {
        this.select_child ( 0 );
      }
    }

    private void change_page ( int delta ) {
      var dest_page = this.get_next_page ( delta );
      uint index = 0;

      if ( delta == -1 ) {
        index = dest_page.length-1;
      }

      dest_page.select_child ( index );

      this.flowbox_pages.set_visible_child ( dest_page );

      if ( this.flowbox_pages.transition_type != Gtk.StackTransitionType.SLIDE_LEFT_RIGHT ) {
        this.flowbox_pages.transition_type = Gtk.StackTransitionType.SLIDE_LEFT_RIGHT;
      }
    }

    private FlowBoxAlbumPage get_next_page ( int delta ) {
      var current_page = this.flowbox_pages.get_visible_child ();
      var dest_page = current_page;

      if ( delta == 1 ) {
        if ( current_page != this.flowbox_pages.get_last_child () ) {
          dest_page = current_page.get_next_sibling ();
        } else {
          dest_page = this.flowbox_pages.get_first_child ();
        }
      } else {
        if ( delta == -1 ) {
          if ( current_page != this.flowbox_pages.get_first_child () ) {
            dest_page = current_page.get_prev_sibling ();
          } else {
            dest_page = this.flowbox_pages.get_last_child ();
          }
        }
      }

      return dest_page as FlowBoxAlbumPage;
    }

    private int get_selected_album_index () {
      var current_page = this.flowbox_pages.get_visible_child () as FlowBoxAlbumPage;
      return current_page.get_selected_child_index ();
    }

    public Album get_album_selected () {
      var current_page = this.flowbox_pages.get_visible_child () as FlowBoxAlbumPage;

      return current_page.get_selected_album ();
    }

    private void on_selected_children_changed (){
      this.album_selected_changed ();
    }
  }
}
