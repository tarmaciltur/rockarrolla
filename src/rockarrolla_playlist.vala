/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_playlist.vala
 *
 * Copyright 2019 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_playlist.ui" )]
  public class Playlist : Gtk.Grid {
    [GtkChild]
    private Gtk.ListBox listbox_playlist;
    [GtkChild]
    private Gtk.Label label_songs_left;
    [GtkChild]
    private Gtk.Label label_time_left;
    private int songs_left;
    private int time_left;
    private Gtk.SizeGroup size_group;
    private bool restoring_playlist;

    public signal void not_empty ();

    construct {
      this.restoring_playlist = false;
      this.songs_left = 0;
      this.time_left = 0;
      this.size_group = new Gtk.SizeGroup ( Gtk.SizeGroupMode.HORIZONTAL );

      this.update_info ();
    }

    public void restore_playlist () {
      var songs = ApplicationWindow.db.select ( "playlist", "song", "ORDER BY time ASC" );

      this.restoring_playlist = true;
      for ( int i = 0; i < songs.length; i++ ) {
        var song = songs.index ( i );

        this.add_song_to_playlist ( song );
      }
      this.restoring_playlist = false;
    }

    private void update_info () {
      this.label_songs_left.set_text ( this.songs_left.to_string () );

      string hour_min_sec = new DateTime.from_unix_utc ( this.time_left ).format ( "%H:%M:%S" );
      this.label_time_left.set_text ( hour_min_sec );
    }

    public void add_song_to_playlist ( string id ) {
      var song = ApplicationWindow.db.select ( "songs,albums", "title,length,artist", "WHERE songs.id = '" + id + "' AND albums.id = songs.album" ).index ( 0 ).split ( "|" );

      var row_song = new SongRow ();

      string min_sec = new DateTime.from_unix_utc ( int.parse ( song[1] ) ).format ( "%M:%S" );

      row_song.set_title ( song[0] );
      row_song.set_artist ( song[2] );
      row_song.set_length ( min_sec );
      row_song.set_id ( id );
      row_song.set_title_size_group ( this.size_group );

      this.listbox_playlist.insert ( row_song, -1 );

      if ( !this.restoring_playlist ) {
        ApplicationWindow.db.insert ( "playlist", "song", "\"" + id + "\"" );
      }

      this.songs_left++;
      this.time_left +=int.parse ( song[1] );

      this.update_info ();
      if ( this.songs_left == 1 ) {
        this.not_empty ();
      }
    }

    public void remove_first_song () {
      var row = this.listbox_playlist.get_row_at_index ( 0 );
      if ( row != null ) {
        var song = row.get_child () as SongRow;
        int length = int.parse ( ApplicationWindow.db.select ( "songs", "length", "WHERE songs.id = '" + song.get_id () + "'" ).index ( 0 ) );
        row.destroy ();

        this.songs_left--;

        if ( this.songs_left == 0 ) {
          this.time_left = 0;
        } else {
          this.time_left -= length;
        }

        this.update_info ();
      }
    }

    public void decrase_one_second () {
      if ( this.time_left > 0 ) {
        this.time_left--;
      }

      this.update_info ();
    }

    public bool has_songs () {
      return this.songs_left > 0;
    }

    public string get_first_song_id () {
      string id = "";
      var row = this.listbox_playlist.get_row_at_index ( 0 );
      if ( row != null ) {
        SongRow song = row.get_child () as SongRow;
        id = song.get_id ();
      }

      return id;
    }
  }
}
