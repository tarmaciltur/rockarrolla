/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_playlist.vala
 *
 * Copyright 2019-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_playlist.ui" )]
  public class Playlist : Gtk.Grid {
    [GtkChild]
    private unowned Gtk.ListView listview;
    [GtkChild]
    private unowned Gtk.Label label_songs_left;
    [GtkChild]
    private unowned Gtk.Label label_time_left;
    private Gtk.SizeGroup size_group;

    construct {
      var selection_model = new Gtk.NoSelection ( new ListStore ( typeof (SongRowProgress) ) );
      this.listview.set_model ( selection_model );

      var factory = new Gtk.SignalListItemFactory ();
      factory.setup.connect ( this.factory_setup );
      factory.bind.connect ( this.factory_bind );
      factory.unbind.connect ( this.factory_unbind );

      this.listview.factory = factory;

      this.size_group = new Gtk.SizeGroup ( Gtk.SizeGroupMode.HORIZONTAL );
    }

    private void factory_setup ( Gtk.ListItem listitem ) {
      var song = new SongRowProgress ();

      song.set_title_size_group ( this.size_group );
      listitem.set_child ( song );
    }

    private void factory_bind ( Gtk.ListItem listitem ) {
      var song = listitem.get_item () as Song;
      var row = listitem.get_child () as SongRowProgress;

      if ( song != null && row != null ) {
        row.set_song ( song );
      }
    }

    private void factory_unbind ( Gtk.ListItem listitem ) {
      var row = listitem.get_child () as SongRowProgress;

      if ( row != null ) {
        row.clear_song ();
      }
    }

    public void update_time ( int time_left, int time_elapsed ) {
      string template = "(<tt>%s</tt>)";
      string hour_min_sec = new DateTime.from_unix_utc ( time_left ).format ( "%H:%M:%S" );

      this.label_time_left.set_markup ( template.printf ( hour_min_sec ) );
      this.update_progress ( time_elapsed );
    }

    public void update_songs ( ListStore songs ) {
      this.get_selection_model ().set_model ( songs );

      var song_in_playlist = this.songs_in_playlist ();

      var text = _("%d songs left" );
      if ( song_in_playlist == 1 ) {
        text = _("%d song left" );
      }
      this.label_songs_left.set_text ( text.printf ( song_in_playlist ) );
    }

    private void update_progress ( int time_elapsed ) {
      var list_item = this.listview.get_first_child ();
      var row = list_item.get_first_child () as SongRowProgress;

      if ( row != null ) {
        row.set_time_elapsed ( time_elapsed );
      }
    }

    public int songs_in_playlist () {
      var ret = 0;
      var row = this.listview.get_first_child ();

      if ( row != null ) {
        ret++;
        var last = this.listview.get_last_child ();

        while ( row != last ) {
          ret++;
          row = row.get_next_sibling ();
        }
      }

      return ret;
    }

    private Gtk.NoSelection get_selection_model () {
      return this.listview.get_model () as Gtk.NoSelection;
    }
  }
}
