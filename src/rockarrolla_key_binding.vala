/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_cover_fetcher.vala
 *
 * Copyright 2019-2020 Fernando Fernandez <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class KeyBinding : Gtk.Widget {
    private Gtk.Text text;
    private uint keyval;

    public signal void changed ( uint keyval );

    static construct {
      set_css_name ( "entry" );
    }

    construct {
      var layout = new Gtk.BinLayout ();
      this.set_layout_manager ( layout );

      this.text = new Gtk.Text ();

      this.text.set_parent ( this );
      var event_controller_key = new Gtk.EventControllerKey ();
      event_controller_key.key_pressed.connect ( this.on_key_press_event );
      event_controller_key.set_propagation_phase ( Gtk.PropagationPhase.TARGET );
      this.text.add_controller ( event_controller_key );
    }

    public uint get_keyval () {
      return this.keyval;
    }

    public void set_keyval ( uint keyval ) {
      this.keyval = Gdk.keyval_to_upper ( keyval );
      this.set_keyval_name ();
      this.changed ( this.keyval );
    }

    private bool on_key_press_event ( uint keyval, uint keycode, Gdk.ModifierType state ) {
      this.set_keyval ( keyval );
      return true;
    }

    private void set_keyval_name () {
      this.text.buffer.set_text ( Gdk.keyval_name ( this.keyval ).data );
    }
  }
}
