/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_songs_list.vala
 *
 * Copyright 2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_songs_viewer.ui" )]
  public class SongsViewer : Gtk.Grid {
    [GtkChild]
    private unowned Gtk.Label label_artist_album;
    [GtkChild]
    public unowned Gtk.ListView listview_album_songs { get; }
    private uint songs_in_list;

    construct {
      var selection_model = new Gtk.SingleSelection ( new ListStore ( typeof (Song) ) );
      selection_model.set_autoselect ( true );
      selection_model.selection_changed.connect ( this.move_scroll );
      this.listview_album_songs.set_model ( selection_model );

      var factory = new Gtk.SignalListItemFactory ();
      factory.setup.connect ( this.factory_setup );
      factory.bind.connect ( this.factory_bind );
      factory.unbind.connect ( this.factory_unbind );

      this.listview_album_songs.factory = factory;
      this.songs_in_list = 0;
    }

    private void factory_setup ( Gtk.ListItem listitem ) {
      var song = new SongRow ();

      listitem.set_child ( song );
    }

    private void factory_bind ( Gtk.ListItem listitem ) {
      var song = listitem.get_item () as Song;
      var row = listitem.get_child () as SongRow;

      if ( song != null && row != null ) {
        row.set_song ( song );
      }
    }

    private void factory_unbind ( Gtk.ListItem listitem ) {
      var row = listitem.get_child () as SongRow;

      if ( row != null ) {
        row.clear_song ();
      }
    }

    public void load_album_songs ( Album album ) {
      this.label_artist_album.set_label ( album.artist + " - " + album.name );

      var songs = album.songs;
      //set null to reset selection
      this.get_selection_model ().set_model ( null );
      this.get_selection_model ().set_model ( songs );

      this.songs_in_list = album.songs_in_album;
    }

    public void move_song_selection ( int delta ) {
      if ( delta != 0 ) {
        var selected = this.get_selection_model ().get_selected ();

        if ( (selected + delta) >= 0 && this.get_selection_model ().get_model ().get_item ( selected + delta ) != null ) {
          this.get_selection_model ().set_selected ( selected + delta );
        }
      }
    }

    public string get_selected_song_id () {
      string ret = "";
      var song = (Song)this.get_selection_model ().get_selected_item ();
      if ( song != null ) {
        ret = song.id;
      }

      return ret;
    }

    private Gtk.SingleSelection get_selection_model () {
      return this.listview_album_songs.get_model () as Gtk.SingleSelection;
    }

    private void move_scroll ( uint position, uint n_items ) {
      var upper = this.listview_album_songs.get_vadjustment ().get_upper ();
      var row_height = upper / this.songs_in_list;
      var current_value = this.listview_album_songs.get_vadjustment ().get_value ();

      var selection = this.get_selection_model ().get_selected ();

      var row_start = row_height * (selection);
      var row_end = row_height * (selection + 1);

      var listview_height = this.listview_album_songs.get_height ();

      if ( row_start < current_value ) {
        this.listview_album_songs.vadjustment.set_value ( row_start );
      } else {
        if ( row_end > current_value + listview_height ) {
          var new_value = row_end - listview_height;
          this.listview_album_songs.vadjustment.set_value ( new_value );
        }
      }
    }
  }
}
