/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_songs_viewer.vala
 *
 * Copyright 2019 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_songs_viewer.ui" )]
  public class SongsViewer : Gtk.Grid {
    [GtkChild]
    private Gtk.Label label_artist_album;
    [GtkChild]
    private Gtk.ListBox listbox_album_songs;
    private int selected_song;

    public SongsViewer () {

    }

    public void load_album_songs ( string album_id ) {
      var album = ApplicationWindow.db.select ( "albums", "artist,name", "WHERE id='" + album_id + "'" ).index ( 0 ).split ( "|" );
      this.label_artist_album.set_label ( album[0] + " - " + album[1] );

      var row = this.listbox_album_songs.get_row_at_index ( 0 );

      while ( row != null ) {
        row.destroy ();
        row = this.listbox_album_songs.get_row_at_index ( 0 );
      }

      var songs = ApplicationWindow.db.select ( "songs", "title,length,id", "WHERE album = '" + album_id + "' ORDER BY track ASC" );

      for ( int i = 0; i < songs.length; i++ ) {
        var song = songs.index ( i ).split ( "|" );

        var row_song = new SongRow ();

        string min_sec = new DateTime.from_unix_utc ( int.parse ( song[1] ) ).format ( "%M:%S" );
        row_song.set_title ( song[0] );
        row_song.set_length ( min_sec );
        row_song.set_id ( song[2] );

        this.listbox_album_songs.insert ( row_song, -1 );
      }

      this.selected_song = -1;

      this.move_song_selection ( 1 );
    }

    public void move_song_selection ( int i ) {
      var child = this.listbox_album_songs.get_row_at_index  ( this.selected_song + i );
      debug ( "move_song_selection 1: " + (this.selected_song + i).to_string () );
      if ( child != null ) {
        var selected = this.listbox_album_songs.get_selected_row ();

        if ( selected != null ) {
          this.listbox_album_songs.unselect_row ( selected );
        }

        this.listbox_album_songs.select_row ( child );

        this.selected_song += i;
        debug ( "move_song_selection 2: " + this.selected_song.to_string () );
      }
    }

    public string get_selected_song_id () {
      string ret = "";
      var child = this.listbox_album_songs.get_row_at_index  ( this.selected_song );
      if ( child != null ) {
        SongRow song = child.get_child () as SongRow;

        ret = song.get_id ();
      }

      return ret;
    }
  }
}
