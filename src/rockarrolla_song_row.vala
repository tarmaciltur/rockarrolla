/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_song_row.vala
 *
 * Copyright 2019 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_song_row.ui" )]
  public class SongRow : Gtk.Grid {
    [GtkChild]
    private Gtk.Label label_title;
    [GtkChild]
    private Gtk.Label label_artist;
    [GtkChild]
    private Gtk.Label label_length;
    private string id;

    public SongRow () {
    }

    public void set_title ( string title ) {
      this.label_title.set_label ( title );
    }

    public void set_artist ( string artist ) {
      this.label_artist.set_label ( " (" + artist + ")" );
    }

    public void set_length ( string length ) {
      this.label_length.set_label ( length );
    }

    public string get_id () {
      return this.id;
    }

    public void set_id ( string id ) {
      this.id = id;
    }

    public void set_title_size_group ( Gtk.SizeGroup size_group ) {
      size_group.add_widget ( this.label_title );
    }
  }
}
