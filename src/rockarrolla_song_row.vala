/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_song_row.vala
 *
 * Copyright 2019-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_song_row.ui" )]
  public class SongRow : Gtk.Grid {
    [GtkChild]
    private unowned Gtk.Label label_title;
    [GtkChild]
    private unowned Gtk.Label label_length;
    private string id;

    public SongRow () {
      this.id = "";
    }

    public void set_song ( Song song ) {
      this.set_title ( song.title );
      this.set_length ( song.length );
    }

    public void clear_song () {
      this.label_title.set_label ( "" );
      this.label_length.set_label ( "" );
    }

    public void set_title ( string title ) {
      this.label_title.set_label ( title );
    }

    public void set_length ( int length ) {
      var min_sec = new DateTime.from_unix_utc ( length ).format ( "%M:%S" );
      this.label_length.set_label ( min_sec );
    }

    public string get_id () {
      return this.id;
    }

    public void set_id ( string id ) {
      this.id = id;
    }

    public void set_title_size_group ( Gtk.SizeGroup size_group ) {
      size_group.add_widget ( this.label_title );
    }
  }
}
