/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_application_window.vala
 *
 * Copyright 2018 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_application_window.ui" )]
  public class ApplicationWindow : Gtk.ApplicationWindow {
    public static Doctrina.BaseDeDatos db;
    public static Doctrina.Infraestructura infra;
    [GtkChild]
    private Gtk.Stack stack_main;
    [GtkChild]
    private PageConfiguration page_configuration;
    [GtkChild]
    private PagePlayer page_player;
    [GtkChild]
    private PageSongsLoader page_songs_loader;

    public ApplicationWindow ( Gtk.Application app ) {
      Object ( application: app );

      Environment.set_prgname ( "rockarrolla" );
      Environment.set_application_name ( "Rockarrolla" );
      Gtk.Window.set_default_icon_name ( "ar.com.softwareperonista.Rockarrolla" );

      ApplicationWindow.infra = new Doctrina.Infraestructura (
        "rockarrolla",
        "3",
        "resource:///ar/com/softwareperonista/rockarrolla/db/rockarrolla_base_de_datos_definicion.sql" );
      ApplicationWindow.db = new Doctrina.BaseDeDatos ( "db.sqlite",
                                                        ApplicationWindow.infra );

      this.page_player.load_albums ();
      this.page_player.restore_playlist ();
    }

    public override bool key_press_event ( Gdk.EventKey event ) {
      if ( event.keyval == Gdk.Key.Right ) {
        debug ( "right" );
        this.page_player.move_album_selection ( 1 );
      }

      if ( event.keyval == Gdk.Key.Left ) {
        debug ( "left" );
        this.page_player.move_album_selection ( -1 );
      }

      if ( event.keyval == Gdk.Key.Up ) {
        debug ( "up" );
        this.page_player.move_song_selection ( -1 );
      }

      if ( event.keyval == Gdk.Key.Down ) {
        debug ( "down" );
        this.page_player.move_song_selection ( 1 );
      }

      if ( event.keyval == Gdk.Key.space ) {
        debug ( "play" );
        this.page_player.add_song_to_playlist ();
      }

      return false;
    }
  }
}
