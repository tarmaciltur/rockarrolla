/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_application_window.vala
 *
 * Copyright 2018-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_application_window.ui" )]
  public class ApplicationWindow : Gtk.ApplicationWindow {
    [GtkChild]
    private unowned Gtk.Stack stack_main;
    [GtkChild]
    private unowned PageConfiguration page_configuration;
    [GtkChild]
    private unowned PagePlayer page_player;
    [GtkChild]
    private unowned PageSongsLoader page_songs_loader;
    private Rockolla rockolla;
    private int previous_height;
    private uint timeout_idle_id;
    private uint timeout_first_load_id;
    private Gtk.EventControllerKey event_controller_key;
    private Settings settings;

    public ApplicationWindow ( Gtk.Application app ) {
      Object ( application: app );
      this.event_controller_key = new Gtk.EventControllerKey ();
      this.event_controller_key.key_pressed.connect ( this.on_key_press_event );
      this.event_controller_key.set_propagation_phase ( Gtk.PropagationPhase.CAPTURE );
      var widget = this as Gtk.Widget;
      widget.add_controller ( this.event_controller_key );

      Gtk.Settings.get_default ().gtk_application_prefer_dark_theme = true;
      Environment.set_prgname ( "rockarrolla" );
      Environment.set_application_name ( "Rockarrolla" );
      Gtk.Window.set_default_icon_name ( "ar.com.softwareperonista.Rockarrolla" );

      // remove field never used warning
      // without the field doesn't find the page
      this.page_configuration.get_visible ();

      var css_provider = new Gtk.CssProvider ();
      css_provider.load_from_resource ( "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla.css" );
      var display = this.get_style_context ().get_display ();
      Gtk.StyleContext.add_provider_for_display ( display, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION );

      this.previous_height = 0;

      this.rockolla = new Rockolla ();
      this.rockolla.albums_load_start.connect ( this.albums_load_start );
      this.rockolla.album_loaded.connect ( this.load_album );
      this.rockolla.albums_load_end.connect ( this.albums_load_end );
      this.rockolla.playlist_empty.connect ( this.playlist_empty );
      this.rockolla.playlist_update_songs.connect ( this.page_player.playlist_update_songs );
      this.rockolla.playlist_update_time.connect ( this.page_player.playlist_update_time );
      this.rockolla.idle_cover_update.connect ( this.page_player.idle_cover_update );
      this.rockolla.loader_update.connect ( this.page_songs_loader.update_progress );
      this.rockolla.collection_reloaded.connect ( this.page_songs_loader.reload_finished );
      this.rockolla.updated_songs_left.connect ( this.page_player.set_songs_left );
      this.rockolla.update_songs_per_dime.connect ( this.page_player.set_songs_per_dime );

      this.rockolla.update_info_labels ();

      this.page_songs_loader.reload_collection.connect ( this.rockolla.reload_collection );

      this.timeout_first_load_id = Timeout.add_seconds ( 1, this.first_load );

      this.settings = new Settings ( "ar.com.softwareperonista.Rockarrolla" );
    }

    private bool first_load () {
      this.rockolla.load_albums ();
      this.rockolla.restore_playlist ();

      this.timeout_idle_id = Timeout.add_seconds ( 30, this.set_idle );
      return GLib.Source.REMOVE;
    }

    private void albums_load_start () {
      this.page_player.albums_load_start ();
      this.page_songs_loader.set_button_reload_sensitive ( false );
    }

    private void load_album ( Album album ) {
      this.page_player.load_album ( album );
    }

    private void albums_load_end () {
      this.page_player.albums_load_end ();
      this.page_songs_loader.set_button_reload_sensitive ( true );
    }

    private bool on_key_press_event ( uint keyval, uint keycode, Gdk.ModifierType state ) {
      this.page_player.set_idle ( false );
      if ( this.timeout_idle_id != 0 ) {
        Source.remove ( this.timeout_idle_id );
        this.timeout_idle_id = 0;
      }
      this.timeout_idle_id = Timeout.add_seconds ( 30, this.set_idle );

      if ( this.stack_main.get_visible_child () == this.page_player ) {
        if ( this.check_key ( keyval, this.settings.get_uint ( "key-right" ) ) ) {
          debug ( "right" );
          this.page_player.move_album_selection ( 1 );
        } else {
          if ( this.check_key ( keyval, this.settings.get_uint ( "key-left" ) ) ) {
            debug ( "left" );
            this.page_player.move_album_selection ( -1 );
          } else {
            if ( this.check_key ( keyval, this.settings.get_uint ( "key-up" ) ) ) {
              debug ( "up" );
              this.page_player.move_song_selection ( -1 );
            } else {
              if ( this.check_key ( keyval, this.settings.get_uint ( "key-down" ) ) ) {
                debug ( "down" );
                this.page_player.move_song_selection ( 1 );
              } else {
                if ( keyval == Gdk.Key.space ) {
                  debug ( "play" );
                  this.rockolla.add_song ( this.page_player.get_selected_song_id () );
                } else {
                  if ( keyval == Gdk.Key.P || keyval == Gdk.Key.p ) {
                    debug ( "pause/play" );
                    this.rockolla.toggle_pause ();
                  } else {
                    if ( keyval == Gdk.Key.S || keyval == Gdk.Key.s ) {
                      debug ( "skip song" );
                      this.rockolla.skip_song ();
                    } else {
                      if ( keyval == Gdk.Key.D || keyval == Gdk.Key.d ) {
                        debug ( "added song to choose" );
                        this.add_songs ();
                      } else {
                        if ( keyval == Gdk.Key.C || keyval == Gdk.Key.c ) {
                          debug ( "changed to configuration page" );
                          this.stack_main.set_visible_child_name ( "page_configuration" );
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        print ( "key %s: %u\n", Gdk.keyval_name ( keyval ), keyval );
        return true;
      }

      return false;
    }

    private void add_songs () {
      this.rockolla.add_songs ();
    }

    [GtkCallback]
    private void hide_headerbar () {
      var headerbar = this.get_titlebar () as Gtk.HeaderBar;
      bool visible = true;

      visible &= this.stack_main.get_visible_child_name () != "page_player";
      visible |= !this.is_maximized ();

      headerbar.set_visible ( visible );
    }

    private bool set_idle () {
      if ( this.rockolla.is_playing () ) {
        this.page_player.set_idle ( true );

        Source.remove ( this.timeout_idle_id );
        this.timeout_idle_id = 0;
      }

      return true;
    }

    private void playlist_empty () {
      this.page_player.set_idle ( false );
      if ( this.timeout_idle_id != 0 ) {
        Source.remove ( this.timeout_idle_id );
        this.timeout_idle_id = 0;
      }
    }

    [GtkCallback]
    private bool on_close_request () {
      this.get_application ().quit ();

      return true;
    }

    private bool check_key ( uint keyval, uint key ) {
      uint lower;
      uint upper;
      Gdk.keyval_convert_case ( keyval, out lower, out upper );

      return (lower == key) || (upper == key);
    }
  }
}
