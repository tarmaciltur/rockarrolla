/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_page_configuration.vala
 *
 * Copyright 2019 Andres Fernandez <andres@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_page_configuration.ui" )]
  public class PageConfiguration : Gtk.Grid {
    [GtkChild]
    private Gtk.FileChooserButton file_chooser_button_songs_directory;
    [GtkChild]
    private Gtk.Entry entry_dime_cost;
    [GtkChild]
    private Gtk.SpinButton spin_button_songs_per_dime;
    private Settings settings;

    construct {
      this.settings = new Settings ( "ar.com.softwareperonista.Rockarrolla" );

      this.load_settings ();
    }

    public void load_settings () {
      var music_directory = settings.get_string ( "music-directory" );

      if ( music_directory == "" ){
        music_directory = Environment.get_user_special_dir ( UserDirectory.MUSIC );
      }

      var file = File.new_for_path ( music_directory );

      try {
        this.file_chooser_button_songs_directory.set_file ( file );
      } catch ( Error e ) {
        stderr.printf ( "Error setting folder: %s", e.message );
      }
      this.file_chooser_button_songs_directory.file_set ();

      var dime_cost = settings.get_string ( "dime-cost" );

      this.entry_dime_cost.set_text ( dime_cost );


      var songs_per_dime = settings.get_int ( "songs-per-dime" );

      this.spin_button_songs_per_dime.set_value ( (double) songs_per_dime );
    }

    [GtkCallback]
    private void save_music_directory () {
      var music_directory = this.file_chooser_button_songs_directory.get_file ().get_path ();

      settings.set_string ( "music-directory", music_directory );
    }

    [GtkCallback]
    private void save_dime_cost () {
      var dime_cost = this.entry_dime_cost.get_text ();

      settings.set_string ( "dime-cost", dime_cost );
    }

    [GtkCallback]
    private void save_songs_per_dime () {
      var songs_per_dime = this.spin_button_songs_per_dime.get_value_as_int ();

      settings.set_int ( "songs-per-dime", songs_per_dime );
    }
  }
}
