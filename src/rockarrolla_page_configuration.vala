/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_page_configuration.vala
 *
 * Copyright 2019 Andres Fernandez <andres@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_page_configuration.ui" )]
  public class PageConfiguration : Gtk.Grid {
    [GtkChild]
    private unowned Gtk.Button button_songs_directory;
    [GtkChild]
    private unowned Gtk.Entry entry_dime_cost;
    [GtkChild]
    private unowned Gtk.SpinButton spin_button_songs_per_dime;
    [GtkChild]
    private unowned KeyBinding key_binding_up;
    [GtkChild]
    private unowned KeyBinding key_binding_down;
    [GtkChild]
    private unowned KeyBinding key_binding_left;
    [GtkChild]
    private unowned KeyBinding key_binding_right;
    [GtkChild]
    private unowned KeyBinding key_binding_play;
    [GtkChild]
    private unowned KeyBinding key_binding_dime;
    [GtkChild]
    private unowned KeyBinding key_binding_skip;
    [GtkChild]
    private unowned KeyBinding key_binding_pause;
    [GtkChild]
    private unowned KeyBinding key_binding_configuration;
    private Settings settings;

    construct {
      this.settings = new Settings ( "ar.com.softwareperonista.Rockarrolla" );

      this.load_settings ();
    }

    public void load_settings () {
      var music_directory = settings.get_string ( "music-directory" );

      if ( music_directory == "" ) {
        music_directory = Environment.get_user_special_dir ( UserDirectory.MUSIC );
      }

      var directory = File.new_for_path ( music_directory );

      this.set_directory_button ( directory );

      var dime_cost = settings.get_string ( "dime-cost" );

      this.entry_dime_cost.set_text ( dime_cost );


      var songs_per_dime = settings.get_int ( "songs-per-dime" );

      this.spin_button_songs_per_dime.set_value ( (double)songs_per_dime );

      this.key_binding_up.set_keyval ( this.settings.get_uint ( "key-up" ) );
      this.key_binding_down.set_keyval ( this.settings.get_uint ( "key-down" ) );
      this.key_binding_left.set_keyval ( this.settings.get_uint ( "key-left" ) );
      this.key_binding_right.set_keyval ( this.settings.get_uint ( "key-right" ) );
      this.key_binding_play.set_keyval ( this.settings.get_uint ( "key-play" ) );
      this.key_binding_dime.set_keyval ( this.settings.get_uint ( "key-dime" ) );
      this.key_binding_skip.set_keyval ( this.settings.get_uint ( "key-skip" ) );
      this.key_binding_pause.set_keyval ( this.settings.get_uint ( "key-pause" ) );
      this.key_binding_configuration.set_keyval ( this.settings.get_uint ( "key-configuration" ) );
    }

/*    [GtkCallback]
    private void save_music_directory () {
      var music_directory = this.file_chooser_button_songs_directory.get_file ().get_path ();

      settings.set_string ( "music-directory", music_directory );
    }
 */
    [GtkCallback]
    private void save_dime_cost () {
      var dime_cost = this.entry_dime_cost.get_text ();

      settings.set_string ( "dime-cost", dime_cost );
    }

    [GtkCallback]
    private void save_songs_per_dime () {
      var songs_per_dime = this.spin_button_songs_per_dime.get_value_as_int ();

      settings.set_int ( "songs-per-dime", songs_per_dime );
    }

    private void set_directory_button ( File directory ) {
      var label = _("Select the Music Folder" );
      if ( directory.query_exists () ) {
        label = directory.get_basename ();
      }

      this.button_songs_directory.set_label ( label );
    }

    [GtkCallback]
    private void on_button_songs_directory_clicked () {
      var dialog = new Gtk.FileChooserNative ( _("Select music folder" ),
                                               this.get_ancestor ( typeof(Gtk.Window) ) as Gtk.Window,
                                               Gtk.FileChooserAction.SELECT_FOLDER,
                                               null, null );

      dialog.response.connect ( (response) => {
        if ( response == Gtk.ResponseType.ACCEPT ) {
          var directory = dialog.get_file ();
          this.set_directory_button ( directory );
          this.settings.set_string ( "music-directory", directory.get_path () );
        }
      } );
      dialog.show ();
    }

    [GtkCallback]
    private void save_key_up ( uint keyval ) {
      this.settings.set_uint ( "key-up", keyval );
    }

    [GtkCallback]
    private void save_key_down ( uint keyval ) {
      this.settings.set_uint ( "key-down", keyval );
    }

    [GtkCallback]
    private void save_key_left ( uint keyval ) {
      this.settings.set_uint ( "key-left", keyval );
    }

    [GtkCallback]
    private void save_key_right ( uint keyval ) {
      this.settings.set_uint ( "key-right", keyval );
    }

    [GtkCallback]
    private void save_key_play ( uint keyval ) {
      this.settings.set_uint ( "key-play", keyval );
    }

    [GtkCallback]
    private void save_key_dime ( uint keyval ) {
      this.settings.set_uint ( "key-dime", keyval );
    }

    [GtkCallback]
    private void save_key_skip ( uint keyval ) {
      this.settings.set_uint ( "key-skip", keyval );
    }

    [GtkCallback]
    private void save_key_pause ( uint keyval ) {
      this.settings.set_uint ( "key-pause", keyval );
    }

    [GtkCallback]
    private void save_key_configuration ( uint keyval ) {
      this.settings.set_uint ( "key-configuration", keyval );
    }
  }
}
