/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_song.vala
 *
 * Copyright 2020 Fernando Fernandez <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class Song : Object {
    public string id { get; private set; }
    public string title { get; private set; }
    public int length { get; private set; }
    public string path { get; private set; }
    public string artist { get; private set; }

    public Song ( string id, string title, int length, string path, string artist = "" ) {
      this.id = id;
      this.title = title;
      this.length = length;
      this.path = path;
      this.artist = artist;
    }
  }
}
