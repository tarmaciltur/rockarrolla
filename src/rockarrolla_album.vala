/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_album.vala
 *
 * Copyright 2020 Fernando Fernandez <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class Album : Object {
    public string id { get; private set; }
    public string name { get; private set; }
    public string artist { get; private set; }
    public string cover { get; private set; }
    public uint songs_in_album { get; private set; }
    public ListStore songs { get; private set; }

    public Album ( string id, string name, string artist, string cover ) {
      this.id = id;
      this.name = name;
      this.artist = artist;
      this.cover = cover;

      this.songs = new ListStore ( typeof(Song) );
    }

    public void add_song ( Song new_song ) {
      this.songs.append ( new_song );
    }


    public void set_total_songs ( uint songs_in_album ) {
      this.songs_in_album = songs_in_album;
    }
  }
}
