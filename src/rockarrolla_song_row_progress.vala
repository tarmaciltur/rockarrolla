/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_song_row.vala
 *
 * Copyright 2019-2020 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_song_row_progress.ui" )]
  public class SongRowProgress : Gtk.Widget {
    [GtkChild]
    private unowned Gtk.Label label_artist;
    [GtkChild]
    private unowned SongRow song_row;
    [GtkChild]
    private unowned Gtk.LevelBar level_bar;
    private int length;
    private int time_elapsed;

    construct {
      this.set_name ( "songrowprogress" );
      this.length = 0;
      this.time_elapsed = 0;
      this.level_bar.set_value ( 0 );

      this.destroy.connect ( this.on_destroy );
    }

    public new void set_song ( Song song ) {
      this.set_length ( song.length );
      this.set_artist ( song.artist );
      this.song_row.set_song ( song );
    }

    public new void clear_song () {
      this.label_artist.set_label ( "" );
      this.time_elapsed = 0;
      this.level_bar.set_value ( 0 );

      this.song_row.clear_song ();
    }

    public void set_artist ( string artist ) {
      this.label_artist.set_label ( " (" + artist + ")" );
    }

    public new void set_length ( int length ) {
      this.length = length;
      this.level_bar.set_max_value ( (double)length );
      this.set_time ();
    }

    public void set_time_elapsed ( int time_elapsed ) {
      this.time_elapsed = time_elapsed;
      this.set_time ();

      double progress = (double)this.time_elapsed;

      //When progress == 1 the level_bar progress turns green
      if ( progress == 1 ) {
        progress += 0.1;
      }

      this.level_bar.set_value ( progress );
    }

    private void set_time () {
      var time = this.length - this.time_elapsed;
      if ( time < 0 ) {
        time = 0;
      }

      this.song_row.set_length ( time );
    }

    public int get_time_left () {
      return this.length - this.time_elapsed;
    }

    public void set_title_size_group ( Gtk.SizeGroup size_group ) {
      this.song_row.set_title_size_group ( size_group );
    }

    private void on_destroy () {
      this.song_row.unparent ();
      this.level_bar.unparent ();
    }
  }
}
