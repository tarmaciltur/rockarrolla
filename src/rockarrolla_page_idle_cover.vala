/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_page_idle_cover.vala
 *
 * Copyright 2020 Fernando Fernandez <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_page_idle_cover.ui" )]
  public class PageIdleCover : Gtk.Grid {
    [GtkChild]
    private unowned Gtk.Picture cover;
    [GtkChild]
    private unowned Gtk.Label label_song;
    [GtkChild]
    private unowned Gtk.Label label_artist;
    [GtkChild]
    private unowned Gtk.LevelBar level_bar;
    [GtkChild]
    private unowned Gtk.Label label_time_left;
    [GtkChild]
    private unowned Gtk.Label label_songs_left;
    [GtkChild]
    private unowned Gtk.Label label_total_time_left;


    construct {}

    public void update ( string title, string artist, string path, double length ) {
      this.set_label ( title, artist );
      this.set_cover ( path );
      this.set_length ( length );
    }

    private void set_cover ( string path ) {
      this.cover.set_filename ( path );
      this.cover.set_visible ( this.cover.get_paintable () != null );
    }

    private void set_label ( string title, string artist ) {
      this.label_song.set_label ( title );
      this.label_artist.set_label ( artist );
    }

    private void set_length ( double length ) {
      this.level_bar.set_max_value ( length );
      this.level_bar.set_value ( 0 );
    }

    public void update_time ( double time_elapsed ) {
      var time_left = this.level_bar.get_max_value () - time_elapsed;
      string template = "<tt>%s</tt>";
      string min_sec = new DateTime.from_unix_utc ( (int)time_left ).format ( "%M:%S" );

      this.label_time_left.set_markup ( template.printf ( min_sec ) );
      this.level_bar.set_value ( time_elapsed );
    }

    public void set_songs_left ( int song_in_playlist ) {
      var text = _("%d songs left" );
      if ( song_in_playlist == 1 ) {
        text = _("%d song left" );
      }
      this.label_songs_left.set_text ( text.printf ( song_in_playlist ) );
    }

    public void set_total_time_left ( int total_time_left ) {
      string template = "<tt>%s</tt>";
      string hour_min_sec = new DateTime.from_unix_utc ( total_time_left ).format ( "%H:%M:%S" );

      this.label_total_time_left.set_markup ( template.printf ( hour_min_sec ) );
    }
  }
}
