/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_flowbox_child_album.ui" )]
  public class FlowBoxChildAlbum : Gtk.Grid {
    [GtkChild]
    private Gtk.Image image;
    [GtkChild]
    private Gtk.Label label_artist_name;
    [GtkChild]
    private Gtk.Label label_name;
    private string id;

    public FlowBoxChildAlbum () {}

    public void set_id ( string id ) {
      this.id = id;
    }

    public string get_id () {
      return this.id;
    }

    public void set_image ( string path ) {
      if ( path != "" && Doctrina.Infraestructura.existe_path ( path ) ) {
        try {
          var pixbuf = new Gdk.Pixbuf.from_file ( path );
          this.set_pixbuf ( pixbuf );
        } catch ( Error e ) {
          error ( "loading image file: %s", e.message );
        }
      } else {
        try {
          var pixbuf = new Gdk.Pixbuf.from_resource ( "/ar/com/softwareperonista/rockarrolla/assets/rockarrolla_no_cover.svg" );
          this.set_pixbuf ( pixbuf );
        } catch ( Error e ) {
          error ( "loading image resource: %s", e.message );
        }
      }
    }

    private void set_pixbuf ( Gdk.Pixbuf pixbuf ) {
      // TODO Do not use magical numbers
      var scaled = pixbuf.scale_simple ( 150, 150, Gdk.InterpType.BILINEAR );

      this.image.set_from_pixbuf ( scaled );
    }

    public void set_artist_name ( string name ) {
      this.label_artist_name.set_label ( name );
    }

    public void set_name ( string name ) {
      this.label_name.set_label ( name );
    }
  }
}
