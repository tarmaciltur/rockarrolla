/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

namespace Rockarrolla {
  [GtkTemplate ( ui = "/ar/com/softwareperonista/rockarrolla/ui/rockarrolla_flowbox_child_album.ui" )]
  public class FlowBoxChildAlbum : Gtk.Grid {
    [GtkChild]
    private unowned Gtk.Picture cover;
    [GtkChild]
    private unowned Gtk.Label label_artist_name;
    [GtkChild]
    private unowned Gtk.Label label_name;
    private string id;
    private bool dummy;
    private Album album;

    public FlowBoxChildAlbum () {
      this.dummy = false;
    }

    public void set_album ( Album album ) {
      this.album = album;
      this.set_artist_name ( album.artist );
      this.set_album_name ( album.name );
      this.set_id ( album.id );
      this.set_cover ( album.cover );
    }

    public Album get_album () {
      return album;
    }

    public void set_id ( string id ) {
      this.id = id;
    }

    public string get_id () {
      return this.id;
    }

    public void set_cover ( string path ) {
      if ( path != "" && Doctrina.Infraestructura.existe_path ( path ) ) {
        this.cover.set_filename ( path );
      } else {
        this.cover.set_resource ( "/ar/com/softwareperonista/rockarrolla/assets/rockarrolla_no_cover.svg" );
      }
    }

    public void set_artist_name ( string name ) {
      this.label_artist_name.set_label ( name );
    }

    public void set_album_name ( string name ) {
      this.label_name.set_label ( name );
    }

    public void set_dummy () {
      this.dummy = true;
      this.set_album_name ( " " );
      this.set_artist_name ( " " );
    }

    public bool is_dummy () {
      return this.dummy;
    }
  }
}
