/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* rockarrolla_cover_fetcher.vala
 *
 * Copyright 2019-2020 Fernando Fernandez <fernando@softwareperonista.com.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rockarrolla {
  public class CoverFetcher {
    private string artist;
    private string album;
    private string music_file_path;
    private string cover_path;
    private Doctrina.Infraestructura infra;

    public CoverFetcher ( Doctrina.Infraestructura infra, string artist, string album, string music_file_path ) {
      this.infra = infra;
      this.artist = artist;
      this.album = album;
      this.music_file_path = music_file_path;
      this.cover_path = "";
    }

    public async string fetch_async () {
      string dir = this.infra.directorio_de_configuracion () + "/albums_covers";
      string filename = CoverFetcher.sha1 ( this.artist + "_" + this.album ) + ".png";

      if ( Doctrina.Infraestructura.existe_path ( dir + "/" + filename ) ) {
        this.cover_path = dir + "/" + filename;
      }

      if ( this.cover_path == "" ) {
        yield this.get_cover_from_folder_async ();
      }

      if ( this.cover_path == "" ) {
        yield this.get_cover_from_file_async ();
      }

      if ( this.cover_path == "" ) {
        yield this.get_cover_from_internet_async ();
      }

      return this.cover_path;
    }

    private async void get_cover_from_folder_async () {
      string album_dir = Path.get_dirname ( this.music_file_path );
      string[] filenames = {"folder.jpg", "Folder.jpg", "album.jpg", "cover.jpg"};

      for ( int i = 0; i < filenames.length; i++ ) {
        var cover = File.new_for_path ( album_dir + "/" + filenames[i] );
        debug ( album_dir + "/" + filenames[i] + ": %s", cover.query_exists ().to_string () );
        if ( cover.query_exists () ) {
          try {
            var pixbuf = new Gdk.Pixbuf.from_file ( album_dir + "/" + filenames[i] );
            this.save_cover_file ( pixbuf );
            break;
          } catch ( Error e ) {
            error ( "loading image file: %s", e.message );
          }
        }

        Idle.add ( this.get_cover_from_folder_async.callback );
        yield;
      }
    }

    private async void get_cover_from_internet_async () {
      string uri_base = "http://images.coveralia.com/audio/";
      string uri_end = "-Frontal.jpg";
      var first_char = this.artist.substring ( 0, 1 ).down ();
      var artist = this.normalize ( this.artist );
      var album = this.normalize ( this.album );

      var uri = uri_base + first_char + "/" + artist + "-" + album + uri_end;

      var cover_data = yield this.get_data_from_url_async ( uri );

      try {
        var loader = new Gdk.PixbufLoader.with_type ( "jpeg" );
        loader.write ( cover_data );
        loader.close ();
        this.save_cover_file ( loader.get_pixbuf () );
      } catch ( Error e ) {
        debug ( "error creating pixbuf: %s", e.message );
      }

      debug ( uri );
    }

    private async void get_cover_from_file_async () {
      var ext_index = this.music_file_path.last_index_of ( "." );
      var extension = this.music_file_path.substring ( ext_index );

      switch ( extension.down () ) {
        case ".mp3":
          yield this.get_cover_from_mp3_async ();
          break;
        case ".flac":
          yield this.get_cover_from_flac_async ();
          break;
      }
    }

    private async void get_cover_from_mp3_async () {
      Bytes data = null;
      try {
        data = yield File.new_for_path ( this.music_file_path ).load_bytes_async ( null, null );
      } catch ( Error e ) {
        debug ( "Error reading file:  %s\n", e.message );
      }

      if ( data != null ) {
        int apic_start = this.get_apic_tag_pos ( data );

        if ( apic_start > 0 ) {

          int frame_data_size = get_apic_frame_size ( data, apic_start+4 );
          if ( frame_data_size > 0 ) {
            const int header_size = 10;
            int cursor = apic_start;
            cursor += header_size;

            //text enconding
            cursor++;

            string mime = "";
            while ( data.get ( cursor ) != '\0' ) {
              mime += data.get ( cursor ).to_string ( "%c" );
              debug ( "data (%d): %s(%c)  -  cursor: %d\n", cursor, data.get ( cursor ).to_string ( "%c" ), data.get ( cursor ), cursor );
              cursor++;
            }
            debug ( "cursor (" + cursor.to_string () + "): " + data.get ( cursor ).to_string ( "%x" ) );
            //mimetype \0 separator
            cursor += 1;
            debug ( "cursor (" + cursor.to_string () + "): " + data.get ( cursor ).to_string ( "%x" ) );
            //pic type
            cursor += 1;
            debug ( "cursor (" + cursor.to_string () + "): " + data.get ( cursor ).to_string ( "%x" ) );

            while ( data.get ( cursor ) != '\0' ) {
              cursor++;
              debug ( "cursor (" + cursor.to_string () + "): " + data.get ( cursor ).to_string ( "%x" ) );
            }

            //description \0 separator
            cursor++;

            int metadata_size = cursor - apic_start - header_size;

            debug ( "metadata_size: " + metadata_size.to_string () );
            if ( cursor < frame_data_size ) {
              int picture_size = frame_data_size - metadata_size - 1;
              int picture_data_begin = ( apic_start + header_size + metadata_size );
              int picture_data_end = ( picture_data_begin + picture_size + 1 );

              debug ( "apic_start: %x  -  cursor: %x  -  frame_data_size: %x  - mime: %s", apic_start, cursor, frame_data_size, mime );
              debug ( "metadata_size: %x, picture_size: %x", metadata_size, picture_size );
              debug ( "picture_data_begin: %x  -  picture_data_end: %x", picture_data_begin, picture_data_end );

              if ( picture_data_end < data.length && (picture_data_begin + picture_data_end) < data.length ) {
                var picture_data = data.slice ( picture_data_begin, picture_data_end );

                try {
                  var loader = new Gdk.PixbufLoader.with_mime_type ( mime );
                  loader.write_bytes ( picture_data );
                  loader.close ();
                  this.save_cover_file ( loader.get_pixbuf () );
                } catch ( Error e ) {
                  debug ( "error creating pixbuf: %s", e.message );
                }
              }
            }
          }
        }
      }
    }

    private int get_apic_tag_pos ( Bytes data ) {
      int ret = 0;
      for ( int i = 0; i < data.length; i++ ) {
        if ( data.get ( i ) == 'A' &&
             data.get ( i+1 ) == 'P' &&
             data.get ( i+2 ) == 'I' &&
             data.get ( i+3 ) == 'C' ) {
          ret = i;
        }
      }

      return ret;
    }

    private int get_apic_frame_size ( Bytes data, int apic_start ) {
      int ret = 0;
      for ( int i = apic_start; i < apic_start+4; i++ ) {
        ret += data.get ( i );
        if ( i < apic_start+3 ) {
          ret <<= 8;
        }
      }

      return ret;
    }

    private async void get_cover_from_flac_async () {
      int cursor = 3;
      bool picture_block = false;
      const int PICTURE = 6;
      const int PICTURE_LAST_BLOCK = 134;
      const int BLOCK_HEADER_SIZE = 4;

      Bytes data = null;
      try {
        data = yield File.new_for_path ( this.music_file_path ).load_bytes_async ( null, null );
      } catch ( Error e ) {
        debug ( "Error reading file:  %s\n", e.message );
      }

      // checking FLAC stream marker
      debug ( data.get ( 0 ).to_string ( "%c" ) +
              data.get ( 1 ).to_string ( "%c" ) +
              data.get ( 2 ).to_string ( "%c" ) +
              data.get ( 3 ).to_string ( "%c" ) );
      if ( data.get ( 0 ) == 'f' &&
           data.get ( 1 ) == 'L' &&
           data.get ( 2 ) == 'a' &&
           data.get ( 3 ) == 'C' ) {
        do {
          cursor++;
          debug ( data.get ( cursor ).to_string ( "%d" )  );
          if ( data.get ( cursor ) == PICTURE || data.get ( cursor ) == PICTURE_LAST_BLOCK ) {
            picture_block = true;
          } else {
            var size = this.get_flac_block_size ( data, cursor+1, 3 );
            //block header size 4 bytes
            cursor += BLOCK_HEADER_SIZE + size-1;
          }
        } while (!picture_block && cursor < data.length );

        if ( picture_block ) {
          // skip header (32bits)
          cursor += BLOCK_HEADER_SIZE;

          // skip picture type (32bits)
          cursor += 4;

          var mime_type_size = this.get_flac_block_size ( data, cursor, 4 );
          // skip length of the MIME type string (32bits)
          cursor += 4;

          debug ( "mime_type_size: " + mime_type_size.to_string () );
          string mime_type = "";
          for ( int i = 0; i < mime_type_size; i++ ) {
            mime_type += data.get ( cursor ).to_string ( "%c" );
            debug ( mime_type );
            cursor++;
          }

          debug ( "mime type: " + mime_type );
          var description_size = this.get_flac_block_size ( data, cursor, 4 );
          // skip length of the description string (32bits)
          cursor += 4;
          // skip description size
          cursor += description_size;

          // skip width (32bits)
          cursor += 4;
          // skip height (32bits)
          cursor += 4;
          // skip color depth (32bits)
          cursor += 4;
          // skip indexed-color pictures (32bits)
          cursor += 4;

          var picture_data_size = this.get_flac_block_size ( data, cursor, 4 );
          // skip picture_data_size (32bits)
          cursor += 4;

          int picture_data_begin = cursor;
          int picture_data_end = cursor + picture_data_size;
          var picture_data = data.slice ( picture_data_begin, picture_data_end );

          try {
            var loader = new Gdk.PixbufLoader.with_mime_type ( mime_type );
            loader.write_bytes ( picture_data );
            loader.close ();
            this.save_cover_file ( loader.get_pixbuf () );
          } catch ( Error e ) {
            debug ( "error creating pixbuf: %s", e.message );
          }
        }
      }
    }

    private int get_flac_block_size ( Bytes data, int start, int bytes_to_read ) {
      int ret = 0;
      for ( int i = start; i < start+bytes_to_read; i++ ) {
        ret += data.get ( i );
        if ( i < start+(bytes_to_read - 1) ) {
          ret <<= 8;
        }
      }
      debug ( "start: %d".printf ( start ) );
      debug ( "bytes to read: %d".printf ( bytes_to_read ) );
      debug ( "size readed: %d".printf ( ret ) );
      return ret;
    }

    private void save_cover_file ( Gdk.Pixbuf pixbuf ) {
      string dir = this.infra.directorio_de_configuracion () + "/albums_covers";
      string filename = CoverFetcher.sha1 ( this.artist + "_" + this.album ) + ".png";

      try {
        this.cover_path = dir + "/" + filename;
        var scaled = pixbuf.scale_simple ( 500, 500, Gdk.InterpType.BILINEAR );
        debug ( this.cover_path );
        scaled.save ( this.cover_path, "png" );
      } catch ( Error e ) {
        debug ( "error while saving pixbuf in a file: %s", e.message );
      }
    }

    private string normalize ( string name ) {
      string ret = name.normalize ();
      ret.canon ( "abcdefghijklmnopqrstuvwxyz" +
                  "012345679" +
                  "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                  '_' );
      return ret;
    }

    public static string sha1 ( string name ) {
      string ret = Checksum.compute_for_string ( ChecksumType.SHA1, name );
      return ret;
    }

    private async uint8[] get_data_from_url_async ( string uri ) {
      var session = new Soup.Session ();
      var message = new Soup.Message ( "GET", uri );
      uint8[] ret = new uint8[1048576];

      try {
        var response = yield session.send_async ( message );
        yield response.read_all_async ( ret, 100, null, null );
      } catch ( Error e ) {
        warning ( e.message );
      }

      return ret;
    }
  }
}
